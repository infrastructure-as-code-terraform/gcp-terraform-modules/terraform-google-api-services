variable "project" {
type = string
default = "mkl-qa"
}
variable "service" {
type = list(string)
default = [
    "cloudresourcemanager.googleapis.com",
    "serviceusage.googleapis.com"
  ]

}
