resource "google_project_service" "api-module" {
  for_each = toset(var.service)
  project = var.project
  service = each.key
}
